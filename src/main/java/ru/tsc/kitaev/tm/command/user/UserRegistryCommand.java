package ru.tsc.kitaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractCommand;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-reg";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "user registry...";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
