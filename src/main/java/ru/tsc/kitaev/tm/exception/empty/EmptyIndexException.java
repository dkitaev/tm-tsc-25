package ru.tsc.kitaev.tm.exception.empty;

import ru.tsc.kitaev.tm.exception.AbstractException;

public final class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Error. Index is empty.");
    }

}
