package ru.tsc.kitaev.tm.exception.empty;

import ru.tsc.kitaev.tm.exception.AbstractException;

public final class EmptyDescriptionException extends AbstractException {

    public EmptyDescriptionException() {
        super("Error. Description is empty.");
    }

}
